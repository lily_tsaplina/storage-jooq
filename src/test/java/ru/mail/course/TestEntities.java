package ru.mail.course;

import db.tables.pojos.Company;
import db.tables.pojos.Nomenclature;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class TestEntities {
    @NotNull
    public static Company company(int n) {
        return new Company(n, "company" + n, "tin" + n, "" + n);
    }

    @NotNull
    public static Nomenclature nomenclature(int n) {
        return new Nomenclature(n, "nomenclature" + n, "" + n);
    }

    public static boolean equals(@Nullable Nomenclature n1, @Nullable Nomenclature n2) {
        if (n1 == n2) {
            return true;
        }
        if (n1 != null && n2 != null) {
            return n1.getId().equals(n2.getId())
                    && n1.getName().equals(n2.getName())
                    && n1.getCode().equals(n2.getCode());
        }
        return false;
    }

    public static <T> boolean equals(@Nullable T obj1, @Nullable T obj2) {
        if (obj1 == obj2) {
            return true;
        }
        if (obj1 != null && obj2 != null) {
            if (obj1.getClass() != obj2.getClass()) {
                return false;
            }
            Field[] fields1 = obj1.getClass().getDeclaredFields();
            Field[] fields2 = obj1.getClass().getDeclaredFields();
            for (int i = 0; i < fields1.length; i++) {
                fields1[i].setAccessible(true);
                fields2[i].setAccessible(true);
                try {
                    if (!fields1[i].get(obj1).equals(fields2[i].get(obj2))) {
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                fields1[i].setAccessible(false);
                fields2[i].setAccessible(false);
            }
            return true;
        }
        return false;
    }

    public static <T> boolean hasItem(@NotNull List<T> list, @NotNull T item) {
        for (T el : list) {
            if (equals(el, item)) {
                return true;
            }
        }
        return false;
    }

    public static <T> boolean hasItems(@NotNull List<T> list, @NotNull T[] items) {
        Boolean[] has = new Boolean[items.length];
        Arrays.fill(has, false);
        for (T el : list) {
            for (int i = 0; i < items.length; i++) {
                if (equals(el, items[i])) {
                    has[i] = true;
                }
            }
        }
        return Arrays.stream(has).anyMatch(hasi -> !hasi);
    }
}
