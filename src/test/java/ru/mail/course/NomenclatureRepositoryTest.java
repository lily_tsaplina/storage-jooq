package ru.mail.course;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.opentable.db.postgres.embedded.ConnectionInfo;
import com.opentable.db.postgres.embedded.FlywayPreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import db.tables.pojos.Company;
import db.tables.pojos.Nomenclature;
import org.hamcrest.Matchers;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import ru.mail.course.dto.CountPriceSum;
import ru.mail.course.repository.TestModule;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public final class NomenclatureRepositoryTest {
    @SuppressWarnings("NullableProblems")
    @NotNull
    @Inject
    private ru.mail.course.NomenclatureRepository nomRep;

    @Rule
    @NotNull
    public final PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(FlywayPreparer.forClasspathLocation("db/migration"));

    @Before
    public void setUp() throws SQLException {
        ConnectionInfo connectionInfo = db.getConnectionInfo();
        ru.mail.course.DBConnetionData connetionData = new ru.mail.course.DBConnetionData(
                "jdbc:postgresql://localhost:" + connectionInfo.getPort() + "/" + connectionInfo.getDbName(),
                "postgres",
                "postgres"
        );
        Injector injector = Guice.createInjector(new TestModule(connetionData));
        injector.injectMembers(this);
    }

    @Test
    public void getAvgPriceForPeriodReturnsCorrectValue() throws SQLException {
        Date fromDate = new GregorianCalendar(2019, Calendar.JANUARY, 2).getTime();
        Date toDate = new GregorianCalendar(2019, Calendar.JANUARY, 5).getTime();
        Timestamp from = new Timestamp(fromDate.getTime());
        Timestamp to = new Timestamp(toDate.getTime());
        BigDecimal expected = BigDecimal.valueOf(7.5);
        BigDecimal actual = nomRep.getAvgPriceForPeriod(from, to);
        actual = actual.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getCountPriceSumsForPeriodByDatesAndTotalReturnsCorrectReport() throws SQLException {
        Date fromDate = new GregorianCalendar(2019, Calendar.JANUARY, 2).getTime();
        Date toDate = new GregorianCalendar(2019, Calendar.JANUARY, 11).getTime();
        Timestamp from = new Timestamp(fromDate.getTime());
        Timestamp to = new Timestamp(toDate.getTime());
        List<CountPriceSum> expected = Arrays.asList(
          new ru.mail.course.dto.CountPriceSum(
                  new Timestamp(new GregorianCalendar(2019, Calendar.JANUARY, 4).getTime().getTime()),
                  15,
                  BigDecimal.valueOf(125)
          ),
          new ru.mail.course.dto.CountPriceSum(
                new Timestamp(new GregorianCalendar(2019, Calendar.JANUARY, 11).getTime().getTime()),
                5,
                BigDecimal.valueOf(25)
          ),
          new ru.mail.course.dto.CountPriceSum(
                null,
                20,
                BigDecimal.valueOf(150)
          )
        );
        List<CountPriceSum> actualReport = nomRep.getCountPriceSumsForPeriodByDatesAndTotal(from, to);
        Assert.assertEquals(actualReport, expected);
    }

    @Test
    public void getCompaniesWithTheirNomenclaturesReturnsCorrectReport() throws SQLException {
        Date fromDate = new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime();
        Date toDate = new GregorianCalendar(2019, Calendar.JANUARY, 11).getTime();
        Timestamp from = new Timestamp(fromDate.getTime());
        Timestamp to = new Timestamp(toDate.getTime());
        Map<Company, Set<Nomenclature>> actualReport = nomRep.getCompaniesWithTheirNomenclatures(from, to);

        Assert.assertEquals(actualReport.get(TestEntities.company(1)), new HashSet<Nomenclature>() {{
            add(TestEntities.nomenclature(1));
            add(TestEntities.nomenclature(2));
            add(TestEntities.nomenclature(3));
            add(TestEntities.nomenclature(4));
            add(TestEntities.nomenclature(5));
        }});
        Assert.assertEquals(actualReport.get(TestEntities.company(3)), new HashSet<Nomenclature>() {{
            add(TestEntities.nomenclature(2));
        }});
        Assert.assertThat(actualReport.keySet(), Matchers.hasItems(
                TestEntities.company(2),
                TestEntities.company(4),
                TestEntities.company(5),
                TestEntities.company(6),
                TestEntities.company(7),
                TestEntities.company(8),
                TestEntities.company(9),
                TestEntities.company(10),
                TestEntities.company(11),
                TestEntities.company(12)
        ));
    }
}
