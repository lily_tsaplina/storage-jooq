package ru.mail.course.repository;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.opentable.db.postgres.embedded.ConnectionInfo;
import com.opentable.db.postgres.embedded.FlywayPreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import db.tables.pojos.Nomenclature;
import org.jetbrains.annotations.NotNull;
import org.jooq.exception.DataAccessException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import repository.NomenclatureCrudRepository;
import ru.mail.course.TestEntities;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static ru.mail.course.TestEntities.hasItem;
import static ru.mail.course.TestEntities.nomenclature;

public final class NomenclatureCrudRepositoryTest {
    @SuppressWarnings("NullableProblems")
    @Inject
    @NotNull
    private NomenclatureCrudRepository repository;

    private int freeId = 20;

    @Rule
    @NotNull
    public final PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(FlywayPreparer.forClasspathLocation("db/migration"));

    @Before
    public void setUp() throws SQLException {
        ConnectionInfo connectionInfo = db.getConnectionInfo();
        ru.mail.course.DBConnetionData connetionData = new ru.mail.course.DBConnetionData(
                "jdbc:postgresql://localhost:" + connectionInfo.getPort() + "/" + connectionInfo.getDbName(),
                "postgres",
                "postgres"
        );
        Injector injector = Guice.createInjector(new TestModule(connetionData));
        injector.injectMembers(this);
    }

    @Test
    public void when_CreateAndFind_Expect_ReturnTheSameNomenclature() {
        Nomenclature nomenclature = nomenclature(freeId++);
        repository.create(nomenclature);
        Nomenclature actual = repository.find(nomenclature.getId());
        repository.delete(nomenclature.getId());
        Assert.assertTrue(TestEntities.equals(nomenclature, actual));
    }

    @Test
    public void when_FindAll_Expect_ReturnsAllEntries() {
        List<Nomenclature> expectedNomenclatures = Arrays.asList(
                nomenclature(freeId++),
                nomenclature(freeId++),
                nomenclature(freeId++)
        );
        for (Nomenclature nomenclature : expectedNomenclatures) {
            repository.create(nomenclature);
        }
        List<Nomenclature> actualNomenclatures = repository.findAll();

        for (Nomenclature nomenclature : expectedNomenclatures) {
            repository.delete(nomenclature.getId());
        }

        Assert.assertEquals(5 + expectedNomenclatures.size(), actualNomenclatures.size());
        for (Nomenclature expectedNomenclature : expectedNomenclatures) {
            Assert.assertTrue(hasItem(actualNomenclatures, expectedNomenclature));
        }
    }

    @Test(expected = DataAccessException.class)
    public void when_CreateExisting_Expect_Exception() {
        Nomenclature nomenclature = nomenclature(freeId++);
        repository.create(nomenclature);
        try {
            repository.create(nomenclature);
        } finally {
            repository.delete(nomenclature.getId());
        }
    }

    @Test
    public void when_Edit_Expect_NewValuesInOnlyThatEntryInDB() {
        Nomenclature nomenclature1 = nomenclature(freeId++);
        Nomenclature nomenclature2 = nomenclature(freeId++);
        repository.create(nomenclature1);
        repository.create(nomenclature2);

        Nomenclature nomenclatureToEdit = repository.find(nomenclature1.getId());
        if (nomenclatureToEdit != null) {
            String newName = nomenclatureToEdit.getName() + "1";
            repository.update(new Nomenclature(
                    nomenclatureToEdit.getId(),
                    newName,
                    nomenclatureToEdit.getCode()
            ));

            Nomenclature editedNomenclature = repository.find(nomenclatureToEdit.getId());

            repository.delete(nomenclature1.getId());
            repository.delete(nomenclature2.getId());

            Assert.assertNotNull(editedNomenclature);
            Assert.assertEquals(nomenclatureToEdit.getId(), editedNomenclature.getId());
            Assert.assertNotEquals(nomenclatureToEdit.getName(), editedNomenclature.getName());
            Assert.assertTrue(hasItem(repository.findAll(), nomenclature2));
        }
    }

    @Test
    public void when_Delete_Expect_DeletedOnlyThatEntry() {
        Nomenclature nomenclature1 = nomenclature(freeId++);
        repository.create(nomenclature1);

        Nomenclature nomenclature2 = nomenclature(freeId++);
        repository.create(nomenclature2);

        repository.delete(nomenclature1.getId());
        List<Nomenclature> expectedNomenclatures = repository.findAll();
        repository.delete(nomenclature2.getId());

        Nomenclature foundNomenclature = repository.find(nomenclature1.getId());
        Assert.assertNull(foundNomenclature);
        Assert.assertTrue(hasItem(expectedNomenclatures, nomenclature2));
    }
}
