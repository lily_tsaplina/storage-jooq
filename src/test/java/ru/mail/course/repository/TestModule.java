package ru.mail.course.repository;

import com.google.inject.AbstractModule;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import repository.NomenclatureCrudRepository;
import repository.impl.NomenclatureCrudRepositoryImpl;
import ru.mail.course.DBConnetionData;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class TestModule extends AbstractModule {
    @Nullable
    private Connection connection;

    public TestModule() throws SQLException {
        this.connection = DriverManager.getConnection(
                "jdbc:postgresql://127.0.0.1:5432/storage",
                "postgres",
                "blossom"
        );
    }

    public TestModule(@NotNull DBConnetionData connectionData) throws SQLException {
        this.connection = DriverManager.getConnection(
                connectionData.getUrl(),
                connectionData.getUser(),
                connectionData.getPassword()
        );
    }

    @Override
    protected void configure() {
        bind(Connection.class).toInstance(connection);
        bind(NomenclatureCrudRepository.class).to(NomenclatureCrudRepositoryImpl.class);
    }
}
