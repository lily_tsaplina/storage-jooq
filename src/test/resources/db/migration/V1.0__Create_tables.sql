CREATE TABLE Company(
  ID int not null,
  NAME varchar(100) not null,
  TIN varchar(10) not null unique ,
  CHECKING_ACCOUNT varchar(20) not null unique
);
ALTER TABLE Company
ADD CONSTRAINT Company_PK PRIMARY KEY (ID);

CREATE TABLE Waybill(
  ID int not null,
  WAYBILL_DATE Date not null,
  COMPANY_ID int not null
);
ALTER TABLE Waybill
ADD CONSTRAINT Waybill_PK PRIMARY KEY(ID),
ADD CONSTRAINT Waybill_Company_Id_FK FOREIGN KEY(COMPANY_ID) REFERENCES Company(ID);

CREATE TABLE Nomenclature(
  ID int not null,
  NAME varchar(200) not null,
  CODE varchar
);
ALTER TABLE Nomenclature
ADD CONSTRAINT Nomenclature_PK PRIMARY KEY(ID);

CREATE TABLE WaybillPosition(
  ID int not null,
  PRICE decimal not null,
  COUNT int not null,
  NOMENCLATURE_ID int not null,
  WAYBILL_ID int not null
);
ALTER TABLE WaybillPosition
ADD CONSTRAINT WaybillPosition_PK PRIMARY KEY(ID),
ADD CONSTRAINT WaybillPosition_NOMENCLATURE_ID_FK FOREIGN KEY(NOMENCLATURE_ID) REFERENCES Nomenclature(ID),
ADD CONSTRAINT WaybillPosition_WAYBILL_ID_FK FOREIGN KEY(WAYBILL_ID) REFERENCES Waybill(ID),
ADD CONSTRAINT WaybillPosition_NOMENCLATURE_IN_WAYBILL_UNIQUE UNIQUE(WAYBILL_ID, NOMENCLATURE_ID);