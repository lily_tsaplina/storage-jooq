package repository.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import repository.CompanyCrudRepository;
import db.tables.pojos.Company;

import java.util.List;

public final class CompanyCrudRepositoryImpl implements CompanyCrudRepository {
    @Override
    public void create(@NotNull Company nomenclature) {

    }

    @Override
    public void update(@NotNull Company nomenclature) {

    }

    @Nullable
    @Override
    public Company find(int id) {
        return null;
    }

    @Override
    public @NotNull List<Company> findAll() {
        return null;
    }

    @Override
    public void delete(int id) {

    }
}
