package repository.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import repository.WaybillCrudRepository;
import db.tables.pojos.Waybill;

import java.util.List;

public class WaybillCrudRepositoryImpl implements WaybillCrudRepository {
    @Override
    public void create(@NotNull Waybill nomenclature) {

    }

    @Override
    public void update(@NotNull Waybill nomenclature) {

    }

    @Nullable
    @Override
    public Waybill find(int id) {
        return null;
    }

    @Override
    public @NotNull List<Waybill> findAll() {
        return null;
    }

    @Override
    public void delete(int id) {

    }
}
