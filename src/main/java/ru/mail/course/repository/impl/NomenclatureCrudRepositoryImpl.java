package repository.impl;

import db.tables.pojos.Nomenclature;
import db.tables.records.NomenclatureRecord;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.impl.DSL;
import org.jooq.DSLContext;
import repository.NomenclatureCrudRepository;

import javax.inject.Inject;
import java.sql.Connection;
import java.util.List;
import java.util.stream.Collectors;

import static db.Tables.NOMENCLATURE;

public final class NomenclatureCrudRepositoryImpl implements NomenclatureCrudRepository {
    @NotNull
    @Inject
    private Connection connection;

    @Override
    public void create(@NotNull Nomenclature nomenclature) {
        DSLContext dslContext = DSL.using(connection);
        NomenclatureRecord nomenclatureRecord = dslContext.newRecord(NOMENCLATURE);
        nomenclatureRecord.setId(nomenclature.getId());
        nomenclatureRecord.setName(nomenclature.getName());
        nomenclatureRecord.setCode(nomenclature.getCode());
        nomenclatureRecord.store();
    }

    @Override
    public void update(@NotNull Nomenclature nomenclature) {
        DSLContext dslContext = DSL.using(connection);
        dslContext.update(NOMENCLATURE)
                .set(NOMENCLATURE.NAME, nomenclature.getName())
                .set(NOMENCLATURE.CODE, nomenclature.getCode())
                .where(NOMENCLATURE.ID.eq(nomenclature.getId()))
                .execute();
    }

    @Nullable
    @Override
    public Nomenclature find(int id) {
        DSLContext dslContext = DSL.using(connection);
        NomenclatureRecord nomenclatureRecord = dslContext.fetchOne(NOMENCLATURE, NOMENCLATURE.ID.eq(id)).into(NOMENCLATURE);
        /*if (nomenclatureRecord.value1() == null) {
            return null;
        }*/
        return new Nomenclature(
                nomenclatureRecord.get(NOMENCLATURE.ID),
                nomenclatureRecord.get(NOMENCLATURE.NAME),
                nomenclatureRecord.get(NOMENCLATURE.CODE)
        );
    }

    @Override
    @NotNull
    public List<Nomenclature> findAll() {
        DSLContext dslContext = DSL.using(connection);
        return dslContext.fetch(NOMENCLATURE).stream()
                .map(record -> new Nomenclature(
                        record.get(NOMENCLATURE.ID),
                        record.get(NOMENCLATURE.NAME),
                        record.get(NOMENCLATURE.CODE)
                        )
                )
                .collect(Collectors.toList());
    }

    @Override
    public void delete(int id) {
        DSLContext dslContext = DSL.using(connection);
        NomenclatureRecord nomenclatureRecord = dslContext.fetchOne(NOMENCLATURE, NOMENCLATURE.ID.eq(id));
        nomenclatureRecord.delete();
    }
}
