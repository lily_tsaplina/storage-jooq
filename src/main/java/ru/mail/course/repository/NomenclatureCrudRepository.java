package repository;

import db.tables.pojos.Nomenclature;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface NomenclatureCrudRepository {
    void create(@NotNull Nomenclature nomenclature);

    void update(@NotNull Nomenclature nomenclature);

    @Nullable Nomenclature find(int id);

    @NotNull List<Nomenclature> findAll();

    void delete(int id);
}
