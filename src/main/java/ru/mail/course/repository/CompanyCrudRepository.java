package repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import db.tables.pojos.Company;

import java.util.List;

public interface CompanyCrudRepository {
    void create(@NotNull Company nomenclature);

    void update(@NotNull Company nomenclature);

    @Nullable Company find(int id);

    @NotNull List<Company> findAll();

    void delete(int id);
}
