package repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import db.tables.pojos.Waybill;

import java.util.List;

public interface WaybillCrudRepository {
    void create(@NotNull Waybill nomenclature);

    void update(@NotNull Waybill nomenclature);

    @Nullable Waybill find(int id);

    @NotNull List<Waybill> findAll();

    void delete(int id);
}
