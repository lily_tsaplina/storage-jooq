package ru.mail.course;

import db.tables.pojos.Company;
import db.tables.pojos.Nomenclature;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Класс для получения отчетов, связанных с поставщиками.
 */
public final class CompanyRepository {
    @NotNull
    private Connection connection;

    @Inject
    public CompanyRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @NotNull
    public List<Company> getFirstTenCompaniesByProductsCount() throws SQLException {
        String query = "SELECT Cmp.id, Cmp.name, Cmp.tin, Cmp.checking_account\n" +
                "        FROM (\n" +
                "            SELECT\n" +
                "              W.company_id,\n" +
                "              SUM(WP.count) AS total_count\n" +
                "            FROM WaybillPosition WP\n" +
                "              JOIN Waybill W ON WP.waybill_id = W.id\n" +
                "            group by W.company_id\n" +
                "            limit 10) S\n" +
                "            JOIN Company Cmp ON Cmp.id = S.company_id\n" +
                "        ORDER BY S.total_count DESC;";

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            return getCompaniesFromResultSet(resultSet);
        }
    }

    /**
     * Получить поставщиков, поставивших заданного товара в количестве, больше заданного для товара порога.
     *
     * @param nomenclaturesToTreasures список пар номенклатура - порог по количеству
     * @return список поставщиков
     * @throws SQLException ошибка выполнения запроса
     */
    @NotNull
    public List<Company> getByProductsCountMoreThan(@NotNull Map<Nomenclature, Integer> nomenclaturesToTreasures) throws SQLException {
        StringBuilder switchTreasure = new StringBuilder("(CASE\n");
        for (Nomenclature nomenclature : nomenclaturesToTreasures.keySet()) {
            switchTreasure.append("WHEN WP.nomenclature_id = ").append(nomenclature.getId()).append("\n")
                    .append("THEN ").append(nomenclaturesToTreasures.get(nomenclature)).append("\n");
        }
        switchTreasure.append("END)\n");

        String nomenclatureIds = nomenclaturesToTreasures.keySet().stream()
                .map(Nomenclature::getId)
                .map(Object::toString)
                .collect(Collectors.joining(", "));

        String query = "SELECT DISTINCT Cmp.id, Cmp.name, Cmp.tin, Cmp.checking_account\n" +
                "FROM (\n" +
                "       SELECT W.company_id\n" +
                "       FROM waybill W JOIN waybillposition WP\n" +
                "           ON W.id = WP.waybill_id\n" +
                "       WHERE WP.nomenclature_id in (" + nomenclatureIds + ")\n" +
                "       GROUP BY W.company_id, WP.nomenclature_id\n" +
                "       HAVING SUM(WP.count) > (\n" +
                switchTreasure +
                " )) S JOIN Company Cmp ON Cmp.id = S.company_id;";
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            return getCompaniesFromResultSet(resultSet);
        }
    }

    @NotNull
    private List<Company> getCompaniesFromResultSet(@NotNull ResultSet resultSet) throws SQLException {
        List<Company> resultList = new ArrayList<>();
        while(resultSet.next()) {
            resultList.add(new Company(
                    resultSet.getInt("id"),
                    resultSet.getString("name"),
                    resultSet.getString("tin"),
                    resultSet.getString("checking_account")

            ));
        }
        return resultList;
    }
}
