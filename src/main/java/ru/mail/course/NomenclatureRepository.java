package ru.mail.course;

import db.tables.pojos.Company;
import db.tables.pojos.Nomenclature;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mail.course.dto.CountPriceSum;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.sql.*;
import java.util.*;

/**
 * Класс для получения отчетов, связанных с номеклатурой.
 */
public final class NomenclatureRepository {
    @Inject
    @NotNull
    @SuppressWarnings("NullableProblems")
    private Connection connection;

    /**
     * Получить среднюю цену товара за заданный период.
     *
     * @param from начало периода
     * @param to конец периода
     * @return средняя цена
     * @throws SQLException ошибка выполнения запроса
     */
    @Nullable
    public BigDecimal getAvgPriceForPeriod(@NotNull Timestamp from, @NotNull Timestamp to) throws SQLException {
        String query = "SELECT AVG(WP.price)\n" +
                "FROM waybill W JOIN waybillposition WP\n" +
                "    ON W.id = WP.waybill_id\n" +
                "WHERE W.waybill_date between ? AND ?;";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setTimestamp(1, from);
            statement.setTimestamp(2, to);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getBigDecimal("avg");
        }
    }

    /**
     * Получить отчет по количеству и стоимости товара за каждый день из периода и итоговые за весь период.
     *
     * @param from начало периода
     * @param to конец периода
     * @return список {@link CountPriceSum}
     * @throws SQLException ошибка выполнения запроса
     */
    @NotNull
    public List<CountPriceSum> getCountPriceSumsForPeriodByDatesAndTotal(
            @NotNull Timestamp from,
            @NotNull Timestamp to
    ) throws SQLException {
        String query = "SELECT W.waybill_date, SUM(WP.count) AS count, SUM(WP.price * WP.count) AS cost\n" +
                "FROM waybill W JOIN waybillposition WP ON W.id = WP.waybill_id\n" +
                "WHERE W.waybill_date between ? AND ?\n" +
                "GROUP BY W.waybill_date\n" +
                "UNION ALL\n" +
                "SELECT null, SUM(WP.count), SUM(WP.price * WP.count)\n" +
                "FROM waybill W JOIN waybillposition WP ON W.id = WP.waybill_id\n" +
                "WHERE W.waybill_date between ? AND ?;";
        List<CountPriceSum> report = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setTimestamp(1, from);
            statement.setTimestamp(2, to);
            statement.setTimestamp(3, from);
            statement.setTimestamp(4, to);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                report.add(new CountPriceSum(
                        resultSet.getTimestamp("waybill_date"),
                        resultSet.getInt("count"),
                        resultSet.getBigDecimal("cost")
                ));
            }
        }
        return report;
    }

    /**
     * Получить список пар: компания -> список поставленных ею товаров за период.
     *
     * @param from дата начала периода
     * @param to дата конца периода
     * @return список пар ({@link Map}): компания -> список поставленных ею товаров за период
     * @throws SQLException ошибка выполнения запроса
     */
    @NotNull
    public Map<Company, Set<Nomenclature>> getCompaniesWithTheirNomenclatures(
            @NotNull Timestamp from,
            @NotNull Timestamp to
    ) throws SQLException {
        String query = "SELECT C.id AS company_id, C.name AS company_name, C.tin, C.checking_account,\n" +
                "  N.id AS nomenclature_id, N.name AS nomenclature_name, N.code\n" +
                "FROM company C\n" +
                "  LEFT JOIN (\n" +
                "    SELECT *\n" +
                "    FROM waybill W\n" +
                "    WHERE W.waybill_date between ? AND ?\n" +
                "            ) W ON W.company_id = C.id\n" +
                "  LEFT JOIN waybillposition WP ON W.id = WP.waybill_id\n" +
                "  LEFT JOIN nomenclature N ON WP.nomenclature_id = N.id\n" +
                "ORDER BY company_id;";
        Map<Company, Set<Nomenclature>> report = new HashMap<>();
        //Map<Company, Nomenclature> queryRes = new Map
        //List<Pair> queryRes = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setTimestamp(1, from);
            statement.setTimestamp(2, to);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int nId = resultSet.getInt("nomenclature_id");
                String nName = resultSet.getString("nomenclature_name");
                String nCode = resultSet.getString("code");
                Nomenclature nomenclature = null;
                Set<Nomenclature> nomenclatures = new HashSet<>();
                if (nName != null && nCode != null) {
                    nomenclature = new Nomenclature(nId, nName, nCode);
                    nomenclatures.add(nomenclature);
                }
                report.merge(
                        new Company(
                                resultSet.getInt("company_id"),
                                resultSet.getString("company_name"),
                                resultSet.getString("tin"),
                                resultSet.getString("checking_account")
                        ),
                        nomenclatures,
                        (list1, list2) -> {
                            list1.addAll(list2);
                            return list1;
                        }
                );
//                queryRes.put(
//                        new Company(
//                                resultSet.getInt("company_id"),
//                                resultSet.getString("company_name"),
//                                resultSet.getString("tin"),
//                                resultSet.getString("checking_account")
//                        ),
//                        nomenclature
//                );
            }
            /*for (Map.Entry<Company, Nomenclature> companyNomenclatureEntry : queryRes.entrySet()) {
                List<Nomenclature> nomenclatures = new ArrayList<>();
                if (companyNomenclatureEntry.getValue() != null) {
                    nomenclatures.add(companyNomenclatureEntry.getValue());
                }
                report.merge(companyNomenclatureEntry.getKey(), nomenclatures,
                        (list1, list2) -> {
                            list1.addAll(list2);
                            return list1;
                        }
                );
            }*/
        }

        return report;
    }
}
